## MiniX7

[_Click to view my **Minix7**_](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX7/)

[_Click to view my **Code**_](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX7/sketch.js)


![Skærmbillede_2022-04-04_kl._00.19.10](/uploads/4e17952d1d441430d480adbe4dbe0eaf/Skærmbillede_2022-04-04_kl._00.19.10.png)

### **My program**
My program is the Olympic rings with a moving landscape that is supposed to look like mountains.  

### **Why I choose this miniX**
I wanted to reworked my first every MiniX, where I didn’t know anything about coding. The time made this, was when i was skiing, so I wasn’t even able to attend classes that specific week. So my miniX1 was my first time thinking and writing code in atom. I wanted to see what I could do differently this time. To see my development in this class. Obviously, I’ve developed my abilities when it comes to coding, but I also know that I’ve a lot to learn still. For me, personally, I think it is quite interesting to see to big constrast between my first ever made miniX and my latest. When doing this, I really can see how I’ve bevome better at coding. 

### **What is changed and why**
I thought it was difficult to change anything about the Olympic rings, without making it too abstract or weird, so I decided to keep that part as it is. However, my focus was on making the mountains and creating and neverending landscape. I did this by using the noise of the function and creating a line that would create these kinds of mountains. I also decided to change the background, which is also something new I learned to do a couple of weeks ago, be pre-uploading the image in your folder. Maybe it’s not the biggest change in history, but I feel proud that I was able to create these mountains. If i look back at the time, when I was creating my first ever MiniX1, I would not have been able to do this. 

### **The relation between aesthetic programming and digital culture**
The digital world and culture are sneaking up on us and is now a part of our everyday life. You can see a huge gap between the different generations - how we’re able to understand and use the digital world. From living a primitive life without electricity to self-driving cars using energy are two completely different worlds. We live in a world where the digital culture is one of the most rapidly evolving things ever. Aesthetic programming is a way to raise political and cultural issues in this digital world we live in. We have learned a lot in this course, regarding ethics, morals and so much more. With aesthetic programming, we are able to create artwork that enlightens different kinds of issues. 
