[_Click to view my **Minix4**_](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX4/)

[_Click to view my **Code**_](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX4/sketch.js)

If it doesn't work try this one:

[web editor p5.js](https://editor.p5js.org/Mariabidstrup/sketches/OeDrT-5Wf)

![Skærmbillede_2022-04-04_kl._01.14.05](/uploads/f1e1ccabc7290a7f2dee63512f9757ba/Skærmbillede_2022-04-04_kl._01.14.05.png)

![Skærmbillede_2022-04-04_kl._01.12.53](/uploads/d58aecca819caf84434545f1c18147ed/Skærmbillede_2022-04-04_kl._01.12.53.png)

### **Can you keep a secret?** 
“can you keep a secret?” is an artwork exposing how websites can be deceiving and steal all of your personal data. It’s a little reminder that we do not always have to trust websites blindly. Many people write pieces of information on different websites because it looks credible, only to find out that their personal data has been stolen.
With this artwork, I wanted to make something that is almost passive-aggressive in its message. By writing "Tell me a secret" and then writing "Do not worry. You can trust me", is in my personal opinion a way to show the irony and paint the overall picture of how websites can be deceiving. 
Once you have written a secret and clicked "submit", you’re then being guided to a website, which will take a picture of you and a headline will appear which will inform you, that your secret has been leaked. In addition, I also chose to make another aspect of collecting personal data. At the bottom of the screen, you can see sentences in small print, which states that all your data is being hacked, etc. This should show that we as humans can sometimes disappear away because of what is right in front of us, and forget to read what is written in small print.

### **WHAT HAVE I LEARNT?**

In this week I’ve been introduced to a lot of different syntaxes. I’ve learned how to make the camera work, how input can be a part of your artwork, how you can remove and hide different lines of coding when an interaction takes place, how a bottom can do and be so many different things - the possibilities are endless if you know how to use the different syntaxes. 


 ### **how your program and thinking address the theme of “capture all.”**

My program addresses the theme “Capture all” by making the viewer able to see how deceiving some websites can be. My purpose was to enlighten everyone, that if you give some personal information, it will be stored and if you’re unfortunate enough, your data will be used in unfortunate ways. My intention is not very subtle or discreet, but that was also the intention with this artwork. I want people to know that it is not always safe to just give different websites ect. your personal information. 

### **What are the cultural implications of data capture?**
When discussing the cultural implications of data capture, I personally think that we need to be very careful of what informations we’re giving the different kinds of websites. We live in a world where we have to use the internet a lot, to keep up with society and what is being expected of us - the internet gives us the opportunity to keep up with the rest of the world - it is almost impossible to avoid using a computer in the digital culture we live in. But that also means, that we has to be very careful of what we are sharing or agreeing to on the internet. 


