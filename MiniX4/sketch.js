//declare global variables 
let camcorder;
let button;
let input, greeting, promise;

function setup() {
  createCanvas(640,480);
  background(200,500,500);
  noStroke();

  //making the thing you type in
  input = createInput();
  input.position(225,230,100);

  //Making the headline
    greeting = createElement('h1', 'Tell me a secret');
    greeting.position(200, 150);

  //Making a "false" reassurance
    promise = createElement('h7', 'Do not worry. You can trust me');
    promise.position(205, 210);

//making a bottom, where you can submit your secret
  button = createButton('Submit');
  button.position(255,260);
  button.size(80,20);
  button.mousePressed(text1); //refers to the next page when mouse is pressed 

//video.
camcorder = createCapture(VIDEO);
camcorder.size(800,800);
camcorder.hide();

}

function text1() {
button.remove(); //removes the button out of the canvas, when pushed.
input.remove(); // (what is being typed)
greeting.remove(); //"tell me a secret"
promise.remove(); //"promise"

background(0);
image(camcorder, 120, 140,370,200); //makes the camera take a picture.
//text.

let lines1 = ('WARNING!'); //local variable
fill('red');
textSize(90);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines1, 300, 80); //refers to my local variable 

fill(250);
textSize(30);
textAlign(CENTER);
text('YOUR SECRET IS OUT...',300,130); //another way of doing it 

fill(250);
textSize(25);
textAlign(CENTER);
text(input.value(),300,375); // refers to the text that is written by the user 


//the statements underneath is text written in small 
let lines2 = ('An unknown browser is trying to steal your data...// When you pressed submit you agreed to the confidential terms of privacy - all of your data that you have created via this device will be saved by different companies..//// Your data and different information will automatically run though')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines2, 320, 400);

let lines3 = ('different browsers and will be used in different situations..//.. If you ever try to blocked this acces, we will leek all of your data....//// Every time you use your computer, we can capture all of activities... - The camera are always on when using this device. An unknown browser is trying ')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines3, 320, 407);

let lines4 = ('We will always keep an eye on you..// We know know where you live. What you buy. Who you are writhing with.///When pressed submit you agreed to the terms of privacy - all of your data that you have created via this device will be saved by different browers.We are watching you')
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines4, 320, 414);

let lines5 = ('different browsers and will be used in different situations..//.. If you ever try to blocked this acces, we will leek all of your data....//// Every time you use your computer, we can capture all of activities... - The camera are always on when using this device. An unknown browser is trying ')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines5, 320, 421);

let lines6 = ('An unknown browser is trying to steal your data...// When you pressed submit you agreed to the confidential terms of privacy - all of your data that you have created via this device will be saved by different companies..//// Your data and different information will automatic run though')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines6, 320, 428);

let lines7 = ('We will always keep an eye on you..// We know know where you live. What you buy. Who you are writhing with.///When pressed submit you agreed to the terms of privacy - all of your data that you have created via this device will be saved by different browers.We are watching you')
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines7, 320, 435);

let lines8 = ('An unknown browser is trying to steal your data...// When you pressed submit you agreed to the confidential terms of privacy - all of your data that you have created via this device will be saved by different companies..//// Your data and different information will automatic run though')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines8, 320, 442);


let lines9 = ('We will always keep an eye on you..// We know know where you live. What you buy. Who you are writhing with.///When pressed submit you agreed to the terms of privacy - all of your data that you have created via this device will be saved by different browers.We are watching you')
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines9, 320, 449);

let lines10 = ('different browsers and will be used in different situations..//.. If you ever try to blocked this acces, we will leek all of your data....//// Every time you use your computer, we can capture all of activities... - The camera are always on when using this device. An unknown browser is trying ')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines10, 320, 456);

let lines11 = ('An unknown browser is trying to steal your data...// When you pressed submit you agreed to the confidential terms of privacy - all of your data that you have created via this device will be saved by different companies..//// Your data and different information will automatic run though')
fill('red');
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines11, 320, 463);

let lines12 = ('We will always keep an eye on you..// We know know where you live. What you buy. Who you are writhing with.///When pressed submit you agreed to the terms of privacy - all of your data that you have created via this device will be saved by different browers.We are watching you')
textSize(5);
textAlign(CENTER);
textLeading(30); //space between the lines
text(lines12, 320, 470);


}
