//declare global variables
let value1 = 143;
let value2 = 227; //blue
let value3 = 255;

let value4 = 96;
let value5 = 193;//green 
let value6 = 31;

function setup() {
  createCanvas(500, 500); //width and height
  background(0); //making the background black
}

function draw() {

//making the earth
stroke(68,127,171); //the outline of the earth
strokeWeight(5); //the size of the outline 
fill(value1,value2,value3); // filled with blue 
ellipse(250,250,300) //the size of the cirle 

//making the countries
stroke(value4,value5,value6)
fill(value4,value5,value6)//every shapes will become green
//creating points on the canvas and connecting them 

//I started of by making points and afterward converted them into curveVertex
point(200,110);
point(225,115);
point(250,105);
point(200,110);

//By using the 'beginShape' and 'endShape(CLOSE)'
// I can create different shapes, witout them being cennected to each other  
beginShape();
curveVertex(200,110);
curveVertex(225,115);
curveVertex(250,105); //the points that later is converted to curveVertex
curveVertex(200,110);
endShape(CLOSE);

ellipse(200,125,10,5)
ellipse(210,140,15,5)
ellipse(230,124,20,3)

point(255,120);
point(250,128);
point(240,135);
point(255,140);
point(270,140); //the points that later is converted to curveVertex
point(280,135);
point(300,135);
point(280,125);
point(255,120);


beginShape();
curveVertex(255,120);
curveVertex(250,128);
curveVertex(240,135);
curveVertex(255,140);
curveVertex(270,140);
curveVertex(280,135);
curveVertex(300,135);
curveVertex(280,125);
curveVertex(255,120);
endShape(CLOSE);

point(331,130);
point(315,144);
point(290,150);
point(320,160);
point(350,170);
point(337,180);
point(335,190); //the points that later is converted to curveVertex
point(360,200);
point(387,200);
point(375,177);
point(360,150);
point(331,130);

beginShape();
curveVertex(331,130);
curveVertex(315,144);
curveVertex(290,150);
curveVertex(320,160);
curveVertex(350,170);
curveVertex(337,180);
curveVertex(335,190);
curveVertex(360,200);
curveVertex(387,200);
curveVertex(375,177);
curveVertex(360,150);
curveVertex(331,130);
endShape(CLOSE);

beginShape();
curveVertex(127,173);
curveVertex(152,172);
curveVertex(156,166);
curveVertex(160,165);
curveVertex(168,167);
curveVertex(173,165);
curveVertex(171,159);
curveVertex(162,151);
curveVertex(187,151);
curveVertex(208,158);
curveVertex(217,168);
curveVertex(208,177);
curveVertex(227,185);
curveVertex(245,175);
curveVertex(267,165);
curveVertex(291,169);
curveVertex(311,184);
curveVertex(291,193);
curveVertex(279,180);
curveVertex(255,187);
curveVertex(257,202);
curveVertex(289,216);
curveVertex(299,205);
curveVertex(314,199);
curveVertex(344,219);
curveVertex(325,229);
curveVertex(340,241);
curveVertex(314,247);
curveVertex(291,251);
curveVertex(268,270);
curveVertex(234,284);
curveVertex(240,306);
curveVertex(259,311);
curveVertex(273,303);
curveVertex(274,317);
curveVertex(304,307);
curveVertex(342,310);
curveVertex(345,333);
curveVertex(310,359);
curveVertex(297,377);
curveVertex(294,391);
curveVertex(281,394);
curveVertex(281,377);
curveVertex(277,356);
curveVertex(264,340);
curveVertex(253,323);
curveVertex(220,302);
curveVertex(182,284);
curveVertex(163,260);
curveVertex(164,283);
curveVertex(149,260);
curveVertex(137,233);
curveVertex(135,214);
curveVertex(109,202);
curveVertex(127,173);
endShape(CLOSE);

ellipse(287,278,24,5)
ellipse(304,290,12,2)
ellipse(316,282,9,2)
ellipse(354,227,12,2)

beginShape();
curveVertex(399,248);
curveVertex(380,265);
curveVertex(374,295);
curveVertex(384,316);
curveVertex(396,283);
curveVertex(399,248);
endShape(CLOSE);

ellipse(174,139,28,4)
stroke(68,127,171);
strokeWeight(5);
noFill();
ellipse(250,250,300)

}

//conditional statement 
//if mouse is pressed the original colors will change
function mousePressed(){
if(value1 == 143){
  value1 = 37
  value2 = 49 //the new color for blue 
  value3 = 78
}else{
  value1 = 143
  value2 = 227 //the same/old color for blue 
  value3 = 255
}
if(value4== 96){
  value4 = 24;
  value5 = 45; //the new color for green
  value6 = 16;
}else{
  value4 = 96;
  value5 = 193; //the same/old color for green 
  value6 = 31;






}

}
