[_Click to view my **Minix2**_](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX2/)

[_Click to view my **Code**_](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX2/sketch.js)


![Skærmbillede_2022-04-04_kl._01.07.15](/uploads/f508296a994f03d821d0ade7f31d10c0/Skærmbillede_2022-04-04_kl._01.07.15.png)

![Skærmbillede_2022-04-04_kl._01.07.21](/uploads/198b33b09e178e8c2fc258ba3552159f/Skærmbillede_2022-04-04_kl._01.07.21.png)


I have made the earth. For this week's miniX I’ve learned the ‘ let value = 0’, ‘point’, ‘beginShape’, ‘curveVertex’, ‘endShape(CLOSE)’, and ‘function mousePressed’. I first started off by defining my different values, so that I, later on, could use them in my code. Afterward, I made a circle that would be the earth - and then, all the hard work began. I needed to make so many different ‘point’s and later turn them into ‘curveVertex’ to create the different countries. It properly took me about two hours to finish the globe. And my coding didn’t get any easier. I wanted to create the function where you’re able to press the earth and make it a different color - to bring awareness to climate changes. I used many hours trying to figure out how to change the color. 

My goal with this miniX was to include everyone, and what would be more including than making the earth. However I didn’t create all the different countries in the world, so some might find it a bit excluding if their countries are not to be found on the program. But my overall intention was to enlighten everyone on the current problem we as humans face. We live on a planet that cries out for help. The ice sheet is melting, temperatures are rising, and much more. If we do not intervene soon, then it would be too late. With this minix, it was a small attempt to raise awareness of this problem - and to include the inclusive aspect in this program, it is that we must all stand together as one, to make the world a better place.
