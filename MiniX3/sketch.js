//throbber
//declaring my global variable
let x= 20;
function setup() {  //The code in setup() is run once when the program starts.
 //create a drawing canvas
 createCanvas(600,600);
 frameRate (8); //((How fast the circles and loading bar will go ))
}

function draw() { //The code inside the draw() function runs continuously from top to bottom until the program is stopped
  background(243,210,209,200); //By adding other values, I can create alpha values 



//MAKING THE LOADING BAR 
////////////////////////////////////////////

  if (x > 250) { // beginning and end condition for the rect inside the loading bar
    x = 20;
    }

//The push() function saves the current drawing style settings and transformations, while pop() restores these settings
//The indside of the loading bar 
    push();
    rectMode(LEFT); //the position of the rect
    noStroke(); //no line at this rect 
    fill(random(200,200),random(270,200),random(230,250)); //random colors to generate a set of colors 
    rect(176,465,x,30,20); // Using my global value 
    x = x + 2; // how fast the bar is moving - adds 2 
    pop();

//The push() function saves the current drawing style settings and transformations, while pop() restores these settings
//The outside of the loading bar 
    push();
    noFill();//making the inside of the rect transparrent 
    strokeWeight(2) //how thickness around the loading bar
    stroke('white'); //making the line white 
    rectMode(CENTER); //placing the bar at the center of my canvas (the first two parameters as the shape's center point)
    rect(300,480,250,30,20); // the size of the loadidng bar
    pop();

//The push() function saves the current drawing style settings and transformations, while pop() restores these settings
    push();
 let l="LOADING . . ." //The laoding text
 strokeWeight(7); // the thickness of the line 
 stroke(255);
 textSize(25); // the size of the text 
 fill(random(100,200),random(100,200),random(100,200)); //random colors to generate a set of colors
 textAlign(10);
 text(l,240,170,300,300); //using by local variable and adding coordinates 
 pop();



//making the clouds
ellipse(100,100,50)
ellipse(100,100,50)
ellipse(140,80,60)
ellipse(180,100,50)
rect(100,85,80,40) //the bottom of the cloud 


ellipse(472,170,50)
ellipse(435,177,35)
ellipse(510,177,35)
rect(431,175,80,20) //the bottom of the cloud 

//making the circles for the ferris wheel
ellipse(300,300,40);
noFill();
stroke('white');
strokeWeight(7);
ellipse(300,300,150)
strokeWeight(4);
ellipse(300,300,100);
strokeWeight(7);

//making the ground
rect(0,400,600)

//making the lines for the ferris wheel
beginShape();
point(300,300);
point(250,245)
curveVertex(300,300);
curveVertex(250,240);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(228,300);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(378,300);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(250,360);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(350,240);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(350,360);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(240,427);
endShape(CLOSE);

beginShape();
curveVertex(300,300);
curveVertex(350,427);
endShape(CLOSE);

rect(200,425,200,10);

  drawElements();


}

function drawElements() { //the visible shapes and forms 
  let num = 100;
  push();
  //move things to the center (canvas level) 
  // (0,0) i placed at the center instead of the top left corner 
  translate(width/2, height/2); 
// 360/num > degree of each ellipes movement 
//frameCount%num > the remainder that to know 
// whitch one among 99 remainder possible positions 
  let cir = 360/num*(frameCount%num);// local variable
  rotate(radians(cir)); 
  noStroke();
  fill(250);2


//Each rotate function has differnt values, so that the cirles starts at different points 
// In this sample code with the throbber, the use of the function rotate() makes 
// the ellipse rotate through a particular number of degrees.
push();
rotate(30) // the position of the ellipse 
ellipse(10,80,30)
pop();

push();
rotate(60)
ellipse(10,80,30)  //the size of the cirle 
pop();

push();
rotate(90)
ellipse(10,80,30)
pop();

push();
rotate(120)
ellipse(10,80,30)
pop();

push();
rotate(150)
ellipse(10,80,30)
pop();

push();
rotate(180)
ellipse(10,80,30)
pop();

push();
rotate(210)
ellipse(10,80,30)
pop();

push();
rotate(240)
ellipse(10,80,30)
pop();

push();
rotate(270)
ellipse(10,80,30)
pop();


}
