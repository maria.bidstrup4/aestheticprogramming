## MiniX3 THROBBER

[Click here to view my **MiniX3**](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX3/)

[Click here to view my **Code**](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX3/sketch.js)

![Skærmbillede_2022-04-04_kl._01.08.30](/uploads/2ee8cad3684e2d9ae4a86636302e0339/Skærmbillede_2022-04-04_kl._01.08.30.png)

### BASIC DESCRIPTION OF MY MINIX3

***What do you want to explore and/or express?***

For these past two weeks, we've been introduced to some new syntaxes such as `push();`and `pop();` and the loop function. For me personally, I wanted to explore the rotate function to create some kind of loop illusion. I really tried to focus on 


**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

So my goal was to make a Ferris wheel that would be never-ending, and a loading bar that would indicate that something is loading(throbber). First of I started by constructing my loading bar. I started off by defining my x value `let x = 20` before function  setup and then I made the statement under function draw that `if x > 250)` , then I made my rectangle and used my x value `rect(176,465,x,30,20);` and at last I defined how fast the loading bar should move with this syntax `x = x + 2;`. For my Ferris wheel, a big part was made with the syntax `rotate(degrees)`. I wanted my throbber to be kind of fun to look at, so that the user is amused while waiting. I achieved this by making my throbber a fun Ferris wheel, and creating random color changes in my loading bar and text.

**Think about a throbber that you have encountered in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

There exist many different kinds of throbbers. I personally find a throbber really annoying to look at. Their purpose is to provide the user with information that something is loading. A throbber is therefore a way in which it is possible to symbolize a period of time. I connect throbbers to be a frustrating and annoying part of waiting when using a particular kind of software. My overall purpose for this week Minix was to make something enjoyable which also will indicate how much time you have left. 


