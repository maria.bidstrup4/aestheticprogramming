let bottle =[]
var screen = 0;
var y=-20;
var x=200;
var speed = 2;
var score= 0;
let coralreef;

//Loading all of my images
function preload(){
  coralreef = loadImage("giphy.gif");
  ocean=loadImage("Ocean.gif");
  trash = loadImage("trash.png");
  sailboat = loadImage("Sailboat.png");
  gameover = loadImage("gameover.png");

}

function setup() {
  createCanvas(600, 400);
  startScreen() //refers to my start screen 

bottle[0]=new Bottle(trash,0,0,60,40); //refers to my class 
}


function draw() {

	if(screen == 0){
    startScreen() //coralref 
  }else if(screen == 1){
  	gameOn() //the game begins 
  }else if(screen==2){
  	endScreen() //final score 

  }
}

function startScreen(){
		background(96, 157, 255)
    image(coralreef,0,0,600,400);
		fill(255)
    textSize(40);
		textAlign(CENTER);
		text('SAVE THE OCEAN', width / 2, height / 2)
		text('Click to start', width / 2, height / 2 + 40);
		reset();
}


function gameOn(){
	background(0)
  image(ocean,0,0,600,400);
    textSize(15);
  text("Water bottles collected = " + score, 100,30) //the position of th text 

// show() = display the bottle
// new Bottle = generates new bottles
  bottle[0].show() //methods - shows objects behavior
  bottle[0] = new Bottle(trash,x,y,30,40)

  rectMode(CENTER)
  //image, horizontal position, position of the boat
  image(sailboat,mouseX,height-95,100,100)
	y+= speed; //2 is the starting speed 
  //Gameover: if y is bigger than height 
  if(y>height){
  	screen =2
	 }
    // The bootle is in when x-20 and x+80
  if(y>height-10 && x>mouseX-20 && x<mouseX+80){
  	y=-20
    speed+=.5 //adds another 0.5 pace 
    score+= 1 //adds a point 
  }
	if(y==-20){
  	pickRandom(); 

  }
}

function pickRandom(){
	x= random(20,width-20) //the placement of the bottles 
}

function endScreen(){
		background(200,500,500)
      image(gameover,0,0,600,400);
		textAlign(CENTER);
    fill(0);
		text('GAME OVER', width / 2, height / 2)
  	text("SCORE = " + score, width / 2, height / 2 + 20)
		text('Click to play again', width / 2, height / 2 + 40);
}

//making the function to press the screen to acces another page
function mousePressed(){
	if(screen==0){ //start screen 
  	screen=1 // Game on
  }else if(screen==2){ //game score 
  	screen=0 //start screen 
  }
}

function reset(){
	  score=0;
  	speed=2;
  	y=-20;
}


class Bottle{
  // constructor = values that can be applied to the object later
  constructor(_img,_xpos,_ypos,_width,_height){ //initialize the object
    this.img= _img
    this.xpos=_xpos
    this.ypos=_ypos
    this.width=_width
    this.height=_height
  }

show(){
image(this.img,this.xpos,this.ypos,this.width,this.height)

}

}
