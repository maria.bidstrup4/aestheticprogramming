var screen = 0;
var y=-20;
var x=200;
var speed = 2;
var score= 0;
let coralreef;


function preload(){
  coralreef = loadImage("giphy.gif");
  ocean=loadImage("Ocean.gif");
  trash = loadImage("trash.png");
  sailboat = loadImage("Sailboat.png");
  gameover = loadImage("game over.png");

}
function setup() {
  createCanvas(600, 400);

}

function draw() {
	if(screen == 0){
    startScreen()
  }else if(screen == 1){
  	gameOn()
  }else if(screen==2){
  	endScreen()

  }
}

function startScreen(){
		background(96, 157, 255)
    image(coralreef,0,0,600,400);
		fill(255)
    textSize(40);
		textAlign(CENTER);
		text('SAVE THE OCEAN', width / 2, height / 2)
		text('Click to start', width / 2, height / 2 + 40);
		reset();
}

function gameOn(){
	background(0)
  image(ocean,0,0,600,400);
    textSize(15);
  text("Water bottles collected = " + score, 100,30)
  image(trash,x,y,30,40)
  rectMode(CENTER)
  image(sailboat,mouseX,height-100,100,100)
	y+= speed;
  if(y>height){
  	screen =2
	 }
  if(y>height-10 && x>mouseX-20 && x<mouseX+80){
  	y=-20
    speed+=.5
    score+= 1
  }
	if(y==-20){
  	pickRandom();

  }
}

function pickRandom(){
	x= random(20,width-20)
}

function endScreen(){
		background(200,500,500)
      image(gameover,0,0,600,400);
		textAlign(CENTER);
    fill(0);
		text('GAME OVER', width / 2, height / 2)
  	text("SCORE = " + score, width / 2, height / 2 + 20)
		text('Click to play again', width / 2, height / 2 + 40);
}

function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
  	screen=0
  }
}

function reset(){
	  score=0;
  	speed=2;
  	y=-20;
}
