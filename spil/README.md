## MiniX6

[_Click to view my **Minix6**_](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX6/)

[_Click to view my **Code**_](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX6/sketch.js)

![Skærmbillede_2022-04-03_kl._23.55.14](/uploads/dd47a75472b5678093b92b863b1dadee/Skærmbillede_2022-04-03_kl._23.55.14.png)

![Skærmbillede_2022-04-03_kl._23.55.36](/uploads/cb856832d15826b2316b9d7a4278926d/Skærmbillede_2022-04-03_kl._23.55.36.png)

![Skærmbillede_2022-04-03_kl._23.55.50](/uploads/e1d40d1a002cbdd9228fab9bba3d2862/Skærmbillede_2022-04-03_kl._23.55.50.png)

### **How does the game/obejct works?**
I’ve made a game that addresses the climate issue regarding ocean pollution. I’ve always cared about the climate of the world, and what better way to show it than to develop a game about it. Usually, games are supposed to be fun and enjoyable and not a way to raise political or cultural problems. However, I have the opportunity to make something more, than just a simple enjoyable game. I’m able to raise a really critical subject, that we as humans need to take a stand on. Therefore, I’ve made a game where the player is, trying to catch trash/water bottles. Each time the player catches the bottles, the next bottle will be 0.5 faster - I decided to add this function to tell that we need to act fast before it’s too late. When you miss a water bottle = game over - and it shows a tropical foreset inside a water bottle.

### **How i program the objects and their related attributes + methods in the game**
I’ve programmed the objects and their related attributes - and methods in the game using some different syntaxes. First of I wanted to create water bottles falling from the sky. When making a class and defining my object, when using constructer made it possible to add it to my game. I used a lot of different syntaxes to make my code and intention work, such as `show`, `new Bottles`etc. Afterwards, I wanted to create a boat, to catch the falling water bottles. First of i pre-uploaded an existing sailboat, as one of my objects - decided its y and x coordinates, so that if the water bottles ended up in the mouse’s x- and y coordinates, the game would continue and if not, the game would be over. 

### **Characteristics of OOP and the wider implications of abstraction**
How you construct your object in a class, will determine its behavior through the code. When constructing this kind of syntax, it becomes easier to decrease the complexity of the code. The implications of abstractions makes it possible for the user to customize and change the code as you go along without changing your whole code. 


### **A wider cultural context**
As I’ve previously mentioned in the questions above, the wider cultural context in my game is to create awareness of this existing problem. Day by day the ocean is being filled with water bottles, plastic backs, and lots of other kinds of trash which pollutes the ocean, killing ocean life and the coral reef. Once I read, a few years ago, that the coral reef is dying due to climate changes and pollution of the earth. It has stuck with me throughout the years and really made me think about how we as humans are the reason for killing beautiful creations that the earth has created, such as the coral reef. I’ve always wanted to dive and see the coral reef, but maybe it is not possible for me and future generations if we do not take action now.
