
function setup() {
  // put setup code here
  createCanvas(600,350);
  background(190,500,500);

}
function draw() {

  //making the OL rings
  stroke(0,200,500);
  strokeWeight(7);
  noFill(220,500,500);
  ellipse(210,100,80);

  stroke('yellow');
  strokeWeight(7);
  ellipse(260,140,80);

  stroke('black');
  strokeWeight(7);
  ellipse(310,100,80);

  stroke('green');
  strokeWeight(7);
  ellipse(360,140,80);

  stroke('red');
  strokeWeight(7);
  ellipse(410,100,80);

  stroke(200,500,500);
  strokeWeight(2);
  fill(245,500,500);
  rect(0.1,240,600,110);

//creating snowfalskes
  noStroke();
  fill(250);
  ellipse(100,100,10);
  ellipse(10,100,5);
  ellipse(480,100,10);
  ellipse(570,100,5);
  ellipse(25,20,10);
  ellipse(45,60,10);
  ellipse(200,10,6);
  ellipse(300,20,10);
  ellipse(400,40,5);
  ellipse(500,30,5);
  ellipse(570,15,10);
  ellipse(360,10,5);
  ellipse(360,40,10);
  ellipse(259,50,5);
  ellipse(100,30,10);
  ellipse(160,30,8);
  ellipse(500,80,6);

//making mountains
fill(235,500,500);
//triangle(left bottom,line,The top (l/R),The top (up/down); right bottom, line)
triangle(100, 240, 200, 170, 300, 240);
triangle(-100, 240, 80, 100, 200, 240);

triangle(698, 240, 525, 100, 340, 240);

//makeing the snow on the mountains
fill('white');
triangle(150,200, 200, 170, 280, 225);
triangle(20, 150, 80, 100, 200, 240);
triangle(585, 150, 525, 100, 340, 240);

//Making reflection from the OL rings
fill(390,400,180);
triangle(180,185, 200, 170, 280, 225);

fill(230,250,300);
triangle(70, 120, 80, 100, 200, 240);

fill(220,1300,120);
triangle(410,185, 460, 160, 340, 240);

fill(1200,100,100);
triangle(420,180,523,100,500,130);

}
