## MiniX1

![Skærmbillede_2022-04-04_kl._01.03.46](/uploads/61bf1a98958cef4989fe1370b9b467a4/Skærmbillede_2022-04-04_kl._01.03.46.png)

[_Click to view my **Minix1**_](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX1/)

[_Click to view my **Code**_](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX1/sketch.js)


### **What have you produced?**
I've created the Olympic rings and a snow landscape existing of some mountains with snow and reflections from the OL rings. 
I started by creating the `function (setup)` and then created a canvas and a light blue background color. Then I typed in `function draw()`, to get started on my "drawing". First of I wanted to create the OL rings. I started by defining the color of the first ring by typing `stroke` , then I defined the thickness of the stroke line by using `strokeWeight`. By typing `noFill` the different ellipse shape forms will appear as rings instead of circles. Lastly, I created the `ellipse`. This process was created for each of the different OL rings. 

I then created some simple snowflakes by using `noStroke`, `fill`, and `ellipse`. 
For the mountains and the “reflections” from the OL rings, I used `triangle`.

### **How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
It was challenging at first, but the more hours I spent practicing coding, the more I became somewhat familiar with the basic functions. Since I started at Aarhus University I’ve looked forward to this course. I’m in no doubt that this is completely out of my comfort zone, but I’m always ready for a challenge! 
I know that  I still have a lot to learn, but I’m looking forward to expanding my knowledge. The next thing I want to master is to make things move.  

### **How is the coding process different from, or similar to, reading and writing text?**
When I write I usually use google docs or pages. These kinds of platforms will show me if I have spelled something wrong, however when I use Javascript I’m not told which mistakes I’m making, the only thing that can show me if I’ve done something wrong is when I press “atom-live-server” and run the code. If there’s no problem with the code it will show me an image, if I’ve done something wrong it will show me a blank page. 

### **What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**
Code and programming are two things I’ve never done before. The assigned reading is a great way to get acquainted when coding before you actually try to execute it yourself. Especially the chapter “Getting Started” by Winnie Soon - was very helpful to make me understand how to write and read code. 
