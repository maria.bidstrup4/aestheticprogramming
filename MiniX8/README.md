## MiniX8 - E-LIT

Our program is available on Josefine Stenhøj's gitlab

[See our program](https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX8/)

[See the code repository](https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX8/sketch.js)

