**ALGORITHMIC PROCEDURES**

**MINIX9 - Flowcharts**
​

**INDIVIDUAL WORK**
I've chosen to make my own flowchart of my miniX8, beacuse i've never heard of JSON files before, and I found it to be a fun but also complex way of using code. When making a flowchart it really become clear, how a JSON file is made by it's own and you then later can refer ro it in your code - it becomes a really great way to see the full picture of an miniX. 

![Skærmbillede_2022-05-15_kl._22.23.52](/uploads/069cf5e57616df96a467a73bba10a44e/Skærmbillede_2022-05-15_kl._22.23.52.png)

***You can find the rest of the assignment on Josefine Stenhøjs Readme***

By cliking this link (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/tree/main/MiniX9)
