## MiniX5 
[Click here to view my **MiniX5**](https://maria.bidstrup4.gitlab.io/aestheticprogramming/MiniX5/)

[Click here to view my **Code**](https://gitlab.com/maria.bidstrup4/aestheticprogramming/-/blob/main/MiniX5/sketch.js)


![Skærmbillede_2022-04-04_kl._01.29.57](/uploads/5a27fc5cb1d9c7811bd82c71d9bd1dea/Skærmbillede_2022-04-04_kl._01.29.57.png)


### Firework that keeps on going

For this week’s assignment, I decided that I wanted to show the beauty in purposely randomness. With my MiniX I used minimalistic shapes and a wide aspect of colors to illustrate my intention. I tried to make some kind of firework that keeps on going.



***What are the rules in your generative program?***

1. 1000 new branches will be created every second`var start_branches = 1000;`
2. The ellipse will starting point, is in the middle of the canvas 
3. For every second, the new braches will have a specific set of coordinates `circle(this.x, this.y, random(this.size), random(this.size))`



***Describe how your program performs over time?***

My desired wish was to make fireworks that, when you click on the canvas, there would be created a firework that would stop and fade away, when a number of ellipses were created. I, unfortunately, didn’t have the time or knowledge to make my desired MiniX, so this is what I could come up with. My miniX will keep on going, making new ellipses every second. If you run my code long enough, it will be a lot of small ellipses in random colors.  

***How do the rules produce emergent behavior?***
When making rules, the one who is coding is able to determine the behavior of an object. it is almost as if the one who is coding sets the tone for the artwork, but it is the computer itself that will determine the final product of the generative artwork. It is only by rules that we’re able to create something, we don’t really know the outcome to.


***What role do rules and processes have in your work?***
I’ve thought a lot about this question and I don’t think it plays a big role in my generative artwork - not to the human eye. All my small dots a random each time, not one is the same - but because the dots are so small the differences between every reset of the page can not really be detected by a person. 

***Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?***

In relation to this week’s topic on randomness, I came to realize the beauty of purposely randomness.That made me think about fireworks. When a firework is being made, there goes a lot of thought into it: which colors and which patterns it will create ect. But you cannot control the outcome, only to an extent - and that’s the beauty of it. Even if we’re trying to make it perfect, we can not control the outcome completely. We don’t know where every little spark of firework will go. It’s kind of planning the unplanningeble. The same goes when it comes to coding where randomness is involved. We are the creators, but when using randomness in our code, we don’t know for sure how it will turn out. I remember a discussion in class, regarding how we are able to make a set of rules, but we’re not able to comprehend the outcome - like fireworks. 

