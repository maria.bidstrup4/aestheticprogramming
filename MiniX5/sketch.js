var branches = [];
var start_branches = 1000; // how many branches will be created every second

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(100); // the higest is 60 

 //for - loop: 
 // declare + initialize, starts with 0 
 //condition: 
 //As long as I is less than 1000, 1+ on i 
 // As long as I is less than 1000, new branches will be created
  for (let i = 0; i < start_branches; i++) {
    branches[i] = new Branch();
  }
  background('black');
}

function draw() {
  for (let i = 0; i < branches.length; i++) {
    branches[i].show();
    branches[i].move();
  }

}

class Branch { //object 
  constructor() { // properties to my object 
    this.x = width / 2; // refers to my object - (this) instead of the objects name 
    this.y = height / 2; //the center position 
    this.size = 3 // the size of the cirles 
  }

 
  move() {
    //the coodinates to make my cirles spread eqaully 
    this.x = this.x + random(-20, 20); 
    this.y = this.y + random(-20, 20);

  }

  show() {
    fill(random(250), random(250), random(250)); //the colors 
    noStroke();

    circle(this.x, this.y, random(this.size));
  }

}
